/* Is it possible to create functions A and B so that new A() == new B()?

function A() { ... }
function B() { ... }

let a = new A;
let b = new B;

alert( a == b ); // true  */

let a = {};

function A() {
  return a;
}
function B() {
  return a;
}

alert(new A() == new B());

// if that function has to return an object then new A()==new B() will be true otherwise false
