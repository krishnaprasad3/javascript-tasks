/* Translate border-left-width to borderLeftWidth
importance: 5
Write the function camelize(str) that changes dash-separated words like “my-short-string” into camel-cased “myShortString”.

That is: removes all dashes, each word after dash becomes uppercased.

Examples:

camelize("background-color") == 'backgroundColor';
camelize("list-style-image") == 'listStyleImage';
camelize("-webkit-transition") == 'WebkitTransition';

P.S. Hint: use split to split the string into an array, transform it and join back. */

let camelize = (str) => {
  let splt = str.split("-");
  console.log(splt);
  let mapping = splt.map((item, index) =>
    index > 0 ? item[0].toUpperCase() + item.slice(1) : item
  );
  let arr = mapping.join("");
  console.log(typeof arr);
  return arr;
};

/* let camelize = (str) =>
  str
    .split("-")
    .map((item, index) =>
      index > 0 ? item[0].toUpperCase() + item.slice(1) : item
    )
    .join(""); */

let str = "-webkit-transition";
console.log(camelize(str));
// camelize(str);
