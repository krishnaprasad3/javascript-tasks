/* Create an extendable calculator
importance: 5

Create a constructor function Calculator that creates “extendable” calculator objects.

The task consists of two parts.

First, implement the method calculate(str) that takes a string like "1 + 2" in the format “NUMBER operator NUMBER” (space-delimited) and returns the result. Should understand plus + and minus -.

Usage example:

let calc = new Calculator;

alert( calc.calculate("3 + 7") ); // 10
Then add the method addMethod(name, func) that teaches the calculator a new operation. It takes the operator name and the two-argument function func(a,b) that implements it.

For instance, let’s add the multiplication *, division / and power **:

let powerCalc = new Calculator;
powerCalc.addMethod("*", (a, b) => a * b);
powerCalc.addMethod("/", (a, b) => a / b);
powerCalc.addMethod("**", (a, b) => a ** b);

let result = powerCalc.calculate("2 ** 3");
alert( result ); // 8
No parentheses or complex expressions in this task.
The numbers and the operator are delimited with exactly one space.
There may be error handling if you’d like to add it. */

/* let input = "3 - 2";

let seprt = input.split(" ");
a = +seprt[0];
op = seprt[1];
b = +seprt[2];

if (op == "+") {
  console.log(a + b);
} else if (op == "-") {
  console.log(a - b);
} */

/**
 * Create a constructor function Calculator that creates “extendable” calculator objects.
 *
 * @param {string} str eg:- ("a + b").
 * @return {number}  powerCalc.calculate("2 ** 3")
 */

// function calculate(str) {
//   let seprt = str.split(" ");
//   a = +seprt[0];
//   op = seprt[1];
//   b = +seprt[2];

//   if (op == "+") {
//     return a + b;
//   } else if (op == "-") {
//     return a - b;
//   } else if (op == "/") {
//     return a / b;
//   } else if (op == "*") {
//     return a * b;
//   } else if (op == "**") {
//     return a ** b;
//   }
// }
// alert(calculate("2 - 3"));

function Calculate() {
  this.method = {
    "+": (a, b) => a + b,
    "-": (a, b) => a - b,
  };
  this.calculate = (str) => {
    let seprt = str.split(" ");
    a = +seprt[0];
    op = seprt[1];
    b = +seprt[2];

    if (!this.method[op] || isNaN(a) || isNaN(b)) {
      return NaN;
    }
    return this.method[op](a, b);
  };
  this.addmethod = (oper, functn) => {
    this.method[oper] = functn;
  };
}

let powercal = new Calculate();
console.log(powercal.calculate("2 + 3"));
powercal.addmethod("/", (a, b) => a / b);
console.log(powercal.calculate("2 / 3"));
