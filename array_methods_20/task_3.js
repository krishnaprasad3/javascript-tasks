/* Filter range "in place"
importance: 4
https://javascript.info/array-methods#filter-range-in-place

Write a function filterRangeInPlace(arr, a, b) that gets an array arr and removes from it all values except those that are between a and b. The test is: a ≤ arr[i] ≤ b.

The function should only modify the array. It should not return anything.

For instance:

let arr = [5, 3, 8, 1];

filterRangeInPlace(arr, 1, 4); // removed the numbers except from 1 to 4

alert( arr ); // [3, 1] */

function filterRangeInPlace(arr, a, b) {
  //going through index
  for (let i = 0; i < arr.length; i++) {
    //going through index
    let val = arr[i]; //getting the value in index

    // remove if outside of the interval
    if (val < a || val > b) {
      //if the condition is true
      arr.splice(i, 1); //remove that item from index
      i--; // to again start from the previous index //if there is no i-- that will skip the index between eg: if the array is 5,8,3,1 the result will be 8,3,1 because the i++ starts from the next index
    }
  }
}

let arr = [5, 3, 8, 1];

filterRangeInPlace(arr, 1, 4); // removed the numbers except from 1 to 4

alert(arr); // [3, 1]
