/* Create an object calculator with three methods:

read() prompts for two values and saves them as object properties with names a and b respectively.
sum() returns the sum of saved values.
mul() multiplies saved values and returns the result.
let calculator = {
  // ... your code ...
};

calculator.read();
alert( calculator.sum() );
alert( calculator.mul() ); */

// let calculator = {
//   read() {
//     let a = +prompt("Enter first number", "0");
//     let b = +prompt("Enter second number", "0");
//     return { a, b };
//   },
//   sum() {
//     let value = this.read();

//     return value.a + value.b;
//   },

//   mul() {
//     let value = this.read();

//     return value.a * value.b;
//   },
// };

// console.log(calculator.sum());
// console.log(calculator.mul());

let calculator = {
  sum() {
    return this.a + this.b;
  },

  mul() {
    return this.a * this.b;
  },

  read() {
    this.a = +prompt("a?", 0);
    this.b = +prompt("b?", 0);
  },
};

calculator.read();
alert(calculator.sum());
alert(calculator.mul());
