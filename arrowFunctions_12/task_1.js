Replace Function Expressions with arrow functions in the code below:

function ask(question, yes, no) {
  if (confirm(question)) yes();
  else no();
}

ask(
  "Do you agree?",
  function() { alert("You agreed."); },
  function() { alert("You canceled the execution."); }
);





let ask = (question, yes, no) => {
  confirm(question) ? yes() : no();
};
let yes = () => alert("You agreed.");
let no = () => alert("You canceled the execution.");

ask("Do you agree?", yes, no);
