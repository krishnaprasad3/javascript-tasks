// What is the code below going to output?

alert( null || 2 || undefined ); //answer 2



// What will the code below output?

alert( alert(1) || 2 || alert(3) );// output 1


// What is this code going to show?

alert( 1 && null && 2 );// output null

// What will this code show?

alert( alert(1) && alert(2) );


// What will the result be?

alert( null || 2 && 3 || 4 ); // output 3 becouse presidence of AND is more than OR. 
// so first process will be AND in this case then OR.



