/* Write the code which asks for a login with prompt.

If the visitor enters "Admin", then prompt for a password, if the input is an empty line or Esc – show “Canceled”, if it’s another string – then show “I don’t know you”.

The password is checked as follows:

If it equals “TheMaster”, then show “Welcome!”,
Another string – show “Wrong password”,
For an empty string or cancelled input, show “Canceled”
The schema:


Please use nested if blocks. Mind the overall readability of the code.

Hint: passing an empty input to a prompt returns an empty string ''. Pressing ESC during a prompt returns null */

let message;
// console.log(typeof message);

let person = prompt("Enter username");
// console.log(typeof person);
/* let password = prompt("Enter your password", "");


if (person ?? password ) {
  message = `username-${person} and password _${password}`;
} else {
  message = "Invalid username or password";
} */
/* if ((person != null &&  person != undefined) &&  (password != null &&   password != undefined)  ) {
  message = `username-${person} and password _${password}`;
} else {
  message = "Invalid username or password";
} */

if (person) {
  // any entry makes it true then enter password otherwise cancelled
  let password = prompt("Enter your password", "");
  if (password) {
    // any entry makes it true then verify otherwise cancelled
    if (person === "Admin") {
      if (password === "TheMaster") {
        //  if both are true
        message = "Welcome!";
      } else if (password != "TheMaster") {
        // entry other than password in password session
        message = "Wrong password !!";
      }
    } else if (person != "Admin") {
      // if enters Wrong password
      message = "I don't know you !!";
    }
  } else {
    message = "Cancelled !!!";
  }
} else {
  message = "Cancelled !!!";
}

alert(message)

