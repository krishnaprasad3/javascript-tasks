/* Write an if condition to check that age is between 14 and 90 inclusively.

“Inclusively” means that age can reach the edges 14 or 90. */

let note;
let age = prompt("How old are you?", "");

if (age >= 14 && age <= 90) {
  note = "You are welcome!";
} else {
  note = "Entry denied for your age!!";
}
alert(note);
