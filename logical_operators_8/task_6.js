/* Write an if condition to check that age is NOT between 14 and 90 inclusively.

Create two variants: the first one using NOT !, the second one – without it. */

let note;
let age = Number(prompt("How old are you?", ""));

// if (!(age >= 14 && age <= 90)) {
//   note = " Access denied";
// } else {
//   note = " You are welcome!";
// }
// alert(note);

// With out NOT

if (age < 14 || age > 90) {
  note = "Access denied!";
} else {
  note = " You are welcome! ";
}

alert(note);
