/* Which of these alerts are going to execute?

What will the results of the expressions be inside if(...)? */

if (-1 || 0) alert("first");
if (-1 && 0) alert("second"); //wont show because 0 is will be taken as false

if (null || (-1 && 1)) alert("third");
