// Write the function isEmpty(obj) which returns true if the object has no properties, false otherwise.

// Should work like that:
// true

let isEmpty = (object) => {
  for (key in object) {
    return false;
  }
  return true;
};
let schedule = {};

alert(isEmpty(schedule));

schedule["8:30"] = "get up";

alert(isEmpty (schedule)); 
