/* Create a function multiplyNumeric(obj) that multiplies all numeric property values of obj by 2.

For instance:

// before the call
let menu = {
  width: 200,
  height: 300,
  title: "My menu"
};

multiplyNumeric(menu);

// after the call
menu = {
  width: 400,
  height: 600,
  title: "My menu"
};
Please note that multiplyNumeric does not need to return anything. It should modify the object in-place.

P.S. Use typeof to check for a number here.

Open a sandbox with tests. */

let menu = {
  width: 200,
  height: 300,
  title: "My menu",
};
// console.log(menu.width);
// menu["width"] = 500;
// console.log(menu.width);

let multiplyNumeric = (object) => {
  for (key in object) {
    if (typeof object[key] == "number") {
     object[key] = object[key] * 2;
    }
  }
  return object;
};
multiplyNumeric(menu);
