/* Create a function readNumber which prompts for a number until the visitor enters a valid numeric value.

The resulting value must be returned as a number.

The visitor can also stop the process by entering an empty line or pressing “CANCEL”. In that case, the function should return null.
 */

/* let readNumber = () => {
  let inp = prompt("Enter a value", "");

  if (inp) {
    if (isFinite(inp) == true) {
      alert(inp);
    } else {
    }
  } else {
    alert("Null!!!");
  }
};
readNumber(); */

let readNumber = () => {
  let inp;
  do {
    inp = prompt("Enter a value", "");
  } while (isFinite(inp) != true);

  if (inp === null || inp === "") {
    return null;
  } 
  return inp
};

readNumber();
