// TASK 4
// Use the for loop to output even numbers from 2 to 10.

/* for (i = 2; i <= 10; i++) {
  if (i % 2 == 0) alert(i);
} */

// TASK 5

/* Rewrite the code changing the for loop to while without altering its behavior (the output should stay same).

for (let i = 0; i < 3; i++) {
  alert( `number ${i}!` );
} */

/* let i = 0;
while (i < 3) {
  alert(`number ${i}!`);
  i++;
} */

// TASK 6

/* Write a loop which prompts for a number greater than 100. If the visitor enters another number – ask them to input again.

The loop must ask for a number until either the visitor enters a number greater than 100 or cancels the input/enters an empty line.

Here we can assume that the visitor only inputs numbers. There’s no need to implement a special handling for a non-numeric input in this task. */

/* let input = prompt("Enter a number", "");

if (input) {
  while (input <= 100) {
    input = prompt("Enter a number", "");
  }
} else {
  alert("Cancelled !!");
} */

// or

let input;

do {
  input = prompt("Enter a number", 0);
} while (input <= 100 && input);
