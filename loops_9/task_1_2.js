// task 1
// What is the last value alerted by this code? Why?

// let i = 3;

// while (i) {
//   alert( i-- );
// }
// last alert will be one
// because when value of i = 0 it becomes falsy



// task 2
/* For every loop iteration, write down which value it outputs and then compare it with the solution.

Both loops alert the same values, or not? */

// The prefix form ++i:

// let i = 0;
// while (++i < 5) alert(i); //alert will display 1 to 4

// The postfix form i++

// let i = 0;
// while (i++ < 5) alert( i );// alert show 1 to 5

// both outputs are not same