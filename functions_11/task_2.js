/* The following function returns true if the parameter age is greater than 18.

Otherwise it asks for a confirmation and returns its result.

function checkAge(age) {
  if (age > 18) {
    return true;
  } else {
    return confirm('Did parents allow you?');
  }
}
Rewrite it, to perform the same, but without if, in a single line.

Make two variants of checkAge:

Using a question mark operator ?
Using OR || */

// using ?

/* function checkAge(age) {
  age > 18 ? true : confirm("Did parents allow you?");
}
let old = prompt("enter your age", "");
checkAge(old);
 */

function checkAge(age) {
  age > 18 || confirm("Did parents allow you?");
}
let old = prompt("enter your age", "");
checkAge(old);
