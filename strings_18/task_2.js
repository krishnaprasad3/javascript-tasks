/* Check for spam
importance: 5
Write a function checkSpam(str) that returns true if str contains ‘viagra’ or ‘XXX’, otherwise false.

The function must be case-insensitive:

checkSpam('buy ViAgRA now') == true
checkSpam('free xxxxx') == true
checkSpam("innocent rabbit") == false */

// let checkSpam = (stir) => {
//   let str = stir.toLowerCase();
//   if (str.includes("xxx") || str.includes("viagra")) {
//     return true;
//   } else {
//     return false;
//   }
// };

let checkSpam = (stir) => {
  let str = stir.toLowerCase();
  return str.includes("xxx") || str.includes("viagra") ? true : false;
};
let spam = "buy ViAgRA now";
alert(checkSpam(spam));
