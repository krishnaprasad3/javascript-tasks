// Examine the following code:

const birthday = '18.04.1982';

const age = someCode(birthday);


// Here we have a constant birthday date and the age is calculated from birthday with the help of some code (it is not provided for shortness, and because details don’t matter here).

// Would it be right to use upper case for birthday? For age? Or even for both?

const BIRTHDAY = '18.04.1982'; // make uppercase?

const AGE = someCode(BIRTHDAY); // make uppercase?

// Answer: It would be right to write variable Birthday in uppercase as const because it is a fixed value,DOB won't change for same person.

// variable age will change when year passes so it can be written in lower or camelcase.