// 1.Declare two variables: admin and name.
// 2.Assign the value "John" to name.
// 3.Copy the value from name to admin.
// 4.Show the value of admin using alert (must output “John”).

"use strict";

let userName;
userName = "john";
let admin;
admin = userName;

alert(admin);
