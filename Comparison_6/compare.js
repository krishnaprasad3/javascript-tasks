// What will be the boolean result for these expressions?

5 > 4 // true
"apple" > "pineapple"// true
"2" > "12" //true
undefined == null  // true
undefined === null // undefined
null == "\n0\n" // false
null === +"\n0\n"// false