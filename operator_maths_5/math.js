// What are the final values of all variables a, b, c and d after the code below?

let a = 1,
  b = 1;

let c = ++a; // ? value will be 2
let d = b++; // ?  value will be 1
