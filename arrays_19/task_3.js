/* Calling in an array context
importance: 5
What is the result? Why? */

let arr = ["a", "b"];

arr.push(function () {
  alert(this);
});

arr[2]();

// output will be  a,b, function () { alert(this)}

// bcz the arr is called with function which call the array